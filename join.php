<?php
/**
 * Template Name: Join Page
 */
get_header(); ?>

<div class="clear"></div>
</header>
<!-- / END HOME SECTION  -->

<div id="content" class="site-content">
<div class="container">
  <div class="content-left-wrap col-md-12">
    <div id="primary" class="content-area">
      <main id="main" class="site-main" role="main">
        <div class="section-header">
          <div class="section-legend">
            <article class=" page type-page status-publish ">
              <header class="entry-header">
                <h1 class="entry-title">
                  <?php  echo get_the_title(); ?>
                </h1>
              </header>
            </article>
          </div>
          <!-- .section-legend --> 
        </div>
        <!-- .section-header -->
        
        <div id="join" >
          <div class="container">
            <?php
                        
                                                    // Get Join Level information
                                        $levels = simple_fields_values('level_heading,level_description,level_members,level_cost');
                                        //echo '<pre>';
                                        //print_r($levels);
                                        ?>
            <div class="row" >
              <?php
                                        foreach ($levels as $level){
                                        
                                            $output = '';
                                            $output .= "<h2>" . $level["level_heading"] . "</h2>";	
                                            $output .= "<h3 class='level-description' >" . $level["level_description"] . "</h3>";	
                                            $output .= "<p class='level-members' >" . $level["level_members"] . "</p>";	
                                            $output .= "<p class='level-cost' >" . $level["level_cost"] . "</p>";	
                                            ?>
              
              <!-- Below is repeated member level section -->
              <div class="footer-widget col-xs-12 col-sm-4">
                <aside class="widget footer-widget-footer widget_rss join-section"> <?php echo $output; ?> </aside>
              </div>
              <!-- /footer-widget -->
              <?php } ?>
            </div>
            <!-- /row --> 
          </div>
          <!-- /container --> 
        </div>
        <!-- /join-section -->
        
        <div class="section-header">
          <?php 
						while ( have_posts() ) : the_post(); 
						

							
							?>
          <div class="section-legend">
            <?php get_template_part( 'content', 'page' ); ?>
            <?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || '0' != get_comments_number() ) :
								comments_template();
							endif;
							

						    
						  
						  endwhile; ?>
          </div>
          <!-- /.section-legend --> 
        </div>
        <!-- /.section-header --> 
        
      </main>
      <!-- #main --> 
      
    </div>
    <!-- #primary --> 
    
  </div>
  <!-- .content-left-wrap --> 
  
</div>
<!-- .container -->

<?php get_footer(); ?>