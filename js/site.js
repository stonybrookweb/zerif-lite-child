//Activate box slider for calendar on home page
jQuery(window).load(function(){
  jQuery('.ecs-event-list').bxSlider({
		auto: true,
		autoControls: true,
		adaptiveHeight: true,
		easing: 'swing',
		controls: false,
		speed: 2500,
		pause: 7000

  });
});


jQuery( document ).ready(function($) {
    //console.log( "ready!" );

  //Set initial states to nothing
  var current_list_ul = '';
  var current_list_heading = ''

  $(".linklistcatname").click(function(){

    //First get link of clicked on item

        var myclass = $(this).parent().parent().parent().attr("class");
        //console.log ("initial my class: " + myclass);

        // Now break all classes into an array based on a space
        var myclass_exploded = myclass.split(" ");
        //console.log("exploded: " + myclass_exploded);

        //set up new variable for final class
        var final_class = '';

        //get array length
        var arrayLength = myclass_exploded.length;
        //console.log("array length: " + arrayLength);

        //iterate over array add a period before each class to make into
        //jQuery usable classes
        for (var i = 0; i < arrayLength; i++){
                final_class += ".";
                final_class += myclass_exploded[i];

            };
        //console.log("final class: "  + final_class);

        //Set up variable for opening new UL
        var new_list_ul = final_class + " ul";
        //Set up variable for header
        var new_list_heading = final_class + " .linklistcatname";
        //console.log( new_list_ul);
        //console.log( new_list_heading);


    if(current_list_heading == ''){

       //no list open so just go ahead an open first list
        $(new_list_ul).slideDown('slow');
				$(new_list_heading).removeClass("linkplus");
				$(new_list_heading).addClass("linkminus");

        current_list_heading = new_list_heading;
        current_list_ul = new_list_ul;

    } else if (new_list_heading == current_list_heading) {

	  if($(current_list_heading).hasClass('linkminus')){
		      // list is open so close it
		      $(current_list_heading).removeClass('linkminus');
              $(current_list_heading).addClass("linkplus");
              $(current_list_ul).slideUp('slow');
			  current_list_heading = '';

		  } else {
			//list is closed so open it
			$(new_list_heading).removeClass('linkplus');
			$(new_list_heading).addClass("linkminus");
			$(new_list_ul).slideDown('slow');

			  current_list_heading = new_list_heading;
              current_list_ul = new_list_ul;
		  }


    } else {
     // this scenario assumes new list heading and current list heading are different

		$(current_list_heading).removeClass('linkminus');
		$(current_list_heading).addClass("linkplus");

      //first scroll up
      $("html, body").animate({ scrollTop: 0 }, 'slow', function(){

          //now slide up with a callback of sliding the new one down
          $(current_list_ul).slideUp('slow',function(){
              $(new_list_heading).removeClass('linkplus');
              $(new_list_heading).addClass("linkminus");
              $(new_list_ul).slideDown('slow');
              //reset names
              current_list_heading = new_list_heading;
              current_list_ul = new_list_ul;
              }
        ); // end slideup

      } //end anonymous callback function in animate
     ); //end slide up

    }//end if

}); // End Click

}); // End Ready



// Animate arrow on home page for larger screens
//TODO: hide on mobile devices
var bounceCounter = 0;

jQuery(document).ready(function($) {
var initialMove =   setTimeout(function() {
        $("#more-content-arrow").animate({bottom: "20px"}, 1000, "linear", bounce3times);
 }, 3000);

setInterval(bounce3times, 30000);

function bounce3times(){
	bounceCounter = 0;
	bounceup();
}

function bounceup(){
//bounce up
$("#more-content-arrow").animate({bottom: "40px"}, 400, "linear", bouncedown);
}

function bouncedown(){
//bounce down
$("#more-content-arrow").animate({bottom: "20px"}, 400, "linear", bounceCountIncrease);
}

function bounceCountIncrease(){
	bounceCounter += 1;
	if (bounceCounter < 3) {
		bounceup();
	}
}

});


//Add attribute to make RSS feeds open in new tabs across the site
jQuery(document).ready(function(){
jQuery('.rsswidget').attr('target','_blank');
});
