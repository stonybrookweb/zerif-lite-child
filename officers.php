<?php
/**
 * Template Name: Officers Page
 */
get_header(); ?>

<div class="clear"></div>
</header>
<!-- / END HOME SECTION  -->

<div id="content" class="site-content">
<div class="container">
  <div class="content-left-wrap col-md-12">
    <div id="primary" class="content-area">
      <main id="main" class="site-main" role="main">
        <section id="team" class="our-team">
          <div class="container">
            <div class="section-header">
              <?php 
						while ( have_posts() ) : the_post(); 
							
							?>
              <div class="section-legend">
                <?php get_template_part( 'content', 'page' ); ?>
              </div>
            </div>
            <?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || '0' != get_comments_number() ) :
								comments_template();
							endif;
							
							// Get Officer information
							$officers_array = simple_fields_values('officer_name,officer_title,officer_linkedin,officer_photo');
							//echo '<pre>';
							//print_r($officers_array);
							?>
            <div class="row" >
              <?php
							foreach ($officers_array as $officer){
							
								$output = '<div>';
								$output .= $officer["officer_name"] . "<br>";	
								$output .= $officer["officer_title"] . "<br>";	
								$output .= $officer["officer_linkedin"] . "<br>";	
								$output .= $officer["officer_photo"]["link"]["thumbnail"] . "<br>";	
								$output .= '</div>';
								//echo $output;
								
								$officer_name = $officer["officer_name"];	
								$officer_title = $officer["officer_title"];	
								$officer_linkedin = $officer["officer_linkedin"];	
								$officer_photo = $officer["officer_photo"]["url"];	
								
								
								?>
              
              <!-- Below is repeated team member section -->
              <div class="col-md-4 col-sm-6 col-xs-12 team-box">
                <div class="team-member">
                  <figure class="profile-pic"><img alt="Uploaded image" src="<?php echo $officer_photo; ?> "></figure>
                  <div class="member-details">
                    <h3 class="dark-text red-border-bottom"><?php echo $officer_name; ?></h3>
                    <div class="position"><?php echo $officer_title; ?> </div>
                  </div>
                  <div class="social-icons">
                    <ul>
                      <li><a target="_blank" href="<?php echo $officer_linkedin; ?>" target="_blank""><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                  </div>
                  <div class=""> </div>
                </div>
              </div>
              <!-- above is repeated section -->
              
              <?php }
							
							
						endwhile;
					?>
            </div>
          </div>
        </section>
      </main>
      <!-- #main --> 
      
    </div>
    <!-- #primary --> 
    
  </div>
  <!-- .content-left-wrap --> 
  
</div>
<!-- .container -->

<?php get_footer(); ?>