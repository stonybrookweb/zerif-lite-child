<?php
// define this constant so we don't get sloggy database queries
define('TEMPLATE_URL', get_stylesheet_directory_uri());

function theme_enqueue_scripts() {
	// Enqueue parent style
	$parent_style = 'zerif-lite';
	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
	
	// Enqueue scripts
	wp_enqueue_script( 'site.js', TEMPLATE_URL . '/js/site.js');
	
	//bxSlider Javascript files
	wp_enqueue_script( 'bxslider.js', TEMPLATE_URL . '/js/jquery.bxslider/jquery.bxslider.min.js');
	wp_enqueue_style( 'bxslider.css', TEMPLATE_URL. '/js/jquery.bxslider/jquery.bxslider.css' );
	wp_enqueue_script( 'jquery-easing', TEMPLATE_URL . '/js/jquery.bxslider/plugins/jquery.easing.1.3.js');			
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );


// Registers an editor stylesheet for the theme.
function wpdocs_theme_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}

add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );


//Page Slug Body Class
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
	$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}

add_filter( 'body_class', 'add_slug_body_class' );


//add shortcode for joinboxes on memberhsip page that uses simple fields to create it
add_shortcode('joinbox', 'joinboxes');

function joinboxes(){
	ob_start();
	?>
	<div id="join" >
    <div class="container">

                        <?php
                        
                                                    // Get Join Level information
                                        $levels = simple_fields_values('level_heading,level_description,level_members,level_cost');
                                        //echo '<pre>';
                                        //print_r($levels);
                                        ?>
                        <div class="row" >
                          <?php
                                        foreach ($levels as $level){
                                        
                                            $output = '';
                                            $output .= "<h2 class='text-center'>" . $level["level_heading"] . "</h2>";	
											$output .="<div class='outer-join'>";
                                            $output .= "<p class='level-description' >" . $level["level_description"] . "</p>";	
                                            $output .= "<p class='level-members' >" . $level["level_members"] . "</p>";	
                                            $output .= "<p class='level-cost' >" . $level["level_cost"] . "</p>";	
											$output .= "</div>";
                                            ?>
                          
                                      <!-- Below is repeated member level section -->
                                      <div class="footer-widget col-xs-12 col-sm-4">
                                       <aside class="widget footer-widget-footer widget_rss join-section"> <?php echo $output; ?> </aside>
                                      </div>
                                      <!-- /footer-widget -->
                          <?php } ?>
                        </div>
                        <!-- /row --> 
                      </div>
                      <!-- /container --> 
                    </div>
                    <!-- /join-section -->
                    
<?php
$output_buffer = ob_get_contents();

ob_end_clean();
return $output_buffer;
}